import React from 'react'
import {useDispatch} from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { setNameTrainer } from '../store/slices/trainerName.slice'

const Home = () => {
    const dispatch = useDispatch()
    const navigate = useNavigate()
    
    const handleSubmit = (e) => {
        e.preventDefault()
        dispatch(setNameTrainer(e.target.name.value.trim()))
        e.target.name.value = ''
        navigate('/pokedex')
    }

    return (
        <div className='form'>
            <img src="./images/logo.png" alt="logo pokemon" width={"250px"} max-width={"350px"}/>
            <h2 className='form-h2'>Hi, Trainer</h2>
            <p className='form-p'>For to start, give me your name</p>
            <form onSubmit={handleSubmit}>
                <input id='name' type="text" />
                <button>Start</button>
            </form>
            <>
            <div className="adorno">
                
            </div>
            </>
        </div>
    )
}

export default Home