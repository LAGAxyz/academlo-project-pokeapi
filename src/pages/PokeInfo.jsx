import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import axios from 'axios'

const PokeInfo = () => {

    const {id} = useParams()

    const [poke, setPoke] = useState()
    const [hasError, setHasError] = useState(false)

    useEffect(()=>{
        const url = `https://pokeapi.co/api/v2/pokemon/${id}`

        axios.get(url)
        .then(res => {
            setPoke(res.data)
            setHasError(false)
        })
        .catch(err => {
            console.log(err)
            setHasError(true)
        })
    }, [id])

    if(hasError){
        return <h1>X ESTE POKEMON NO EXISTE X</h1>
    } else {
        return (
            <div>
                <img src={poke?.sprites.other['official-artwork'].front_default} alt="" />
                <h1>{poke?.name}</h1>
            </div>
        )
    }

}

export default PokeInfo
