import React from 'react'
import { useSelector } from 'react-redux'
import axios from 'axios'
import { useEffect } from 'react'
import { useState } from 'react'
import PokeCard from '../../components/Pokedex/PokeCard'
import { useNavigate } from 'react-router-dom'
import SelectTypes from '../../components/Pokedex/SelectTypes'

const Pokedex = () => {
    const { nameTrainer } = useSelector(state => state)

    const [pokemons, setPokemons] = useState()
    const [selectValue, setSelectValue] = useState('allpokemons')

    useEffect(()=>{
        if(selectValue==='allpokemons'){
            // const url = `https://pokeapi.co/api/v2/pokemon?limit=100000&offset=0`
            const url = `https://pokeapi.co/api/v2/pokemon?limit=25&offset=0`
    
            axios.get(url)
            .then(res => {
                setPokemons(res.data)
            })
            .catch(error => {
                console.log(error)
            })
        } else {
            axios.get(selectValue)
            .then(res => {
                const results = res.data.pokemon.map(e => e.pokemon)
                setPokemons({results})
            })
            .catch(err => {
                console.log(err)
            })
        }
    }, [selectValue])

    const navigate = useNavigate()

    const handleSubtmit = (e)=>{
        e.preventDefault()
        const inputValue = e.target.pokemon.value.trim().toLowerCase()
        navigate(`/pokedex/${inputValue}`)
        e.target.pokemon.value = ''
    }

    return (
        <div>
            <h1 className='txttext'><span className='txtname'>Hi {nameTrainer}, </span>here find your favorite pokemon</h1>
            
            <form onSubmit={handleSubtmit} className='form1'>
                <input type="text" id='pokemon'/>
                <button>Search</button>
            </form>

            <SelectTypes setSelectValue={setSelectValue}/>

            <div className='pokeContent'>
                {
                    pokemons?.results.map(pokemon => (
                        <PokeCard
                            key={pokemon.url}
                            pokemon={pokemon}
                        />
                    ))
                }
            </div>
        </div>
    )
}

export default Pokedex