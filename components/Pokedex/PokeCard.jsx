import React from 'react'
import { useEffect } from 'react'
import { useState } from 'react'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'

const PokeCard = ({ pokemon }) => {
    const [poke, setPoke] = useState()

    useEffect(() => {
        axios.get(pokemon.url)
        .then(res => setPoke(res.data))
        .catch(error => console.log(error))
    }, [])

    const navigate = useNavigate()

    const handleClick = ()=>{
        navigate(`/pokedex/${poke.id}`)
    }

    return (
        <article onClick={handleClick} className={`pokemon bg-${poke?.types[0].type.name}`}>
            <header className='header'>
                <img className='headerIMG' src={poke?.sprites.other['official-artwork'].front_default} alt="pokeimg" />
            </header>
            <h2>{poke?.name}</h2>
            <ul className='pokelist poketipo'>
                {
                    poke?.types.map(type => (
                        <li key={type.type.name}>{type.type.name}</li>
                    ))
                }
            </ul>
            {/* <hr /> */}
            <ul className='pokelist pokestadisticas'>
                {
                    poke?.stats.map(stat=>(
                        <li key={stat.stat.url} className='pokeitems'>
                            <span className='txttitle'>{stat.stat.name}</span>
                            <span className='txtvalor'>{stat.base_stat}</span>
                        </li>
                    ))
                }
            </ul>
        </article>
    )
}

export default PokeCard